import {Vue, router} from './base-vue';

import ShopForm from './components/ShopComponents/ShopForm.vue';
import MemberMenu from "./components/PageComponents/MemberMenu";
import store from './store/auth';

Vue.component('member-menu', MemberMenu);
Vue.component('shop-form', ShopForm);

const app = new Vue({
    el: '#app',
    store,
    router
});
