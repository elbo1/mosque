import MainHeader from "./components/PageComponents/MainHeader";
import MainFooter from './components/PageComponents/MainFooter.vue';
import DefaultPage from './components/PageComponents/DefaultPage.vue';
import DefaultForm from './components/PageComponents/DefaultForm.vue';
import DefaultCard from './components/PageComponents/DefaultCard.vue';

require('./bootstrap');

Vue.filter('truncate', function (text, stop, clamp) {
    return text.slice(0, stop) + (stop < text.length ? clamp || '...' : '')
});

import Vue from 'vue';
import Vuetify from 'vuetify';
import router from './routes/router';
import Meta from 'vue-meta';

Vue.use(Vuetify, {
    theme: {
        primary: '#1976D2',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107'
    }
});

Vue.use(Meta, {
    keyName: 'meta',
    refreshOnceOnNavigation: true
});

Vue.component('main-header', MainHeader);
Vue.component('main-footer', MainFooter);
Vue.component('default-page', DefaultPage);
Vue.component('default-card', DefaultCard);
Vue.component('default-form', DefaultForm);

export {Vue, router};