import axios from 'axios';

export default {
    namespaced: true,
    state: {
        reviews: []
    },
    actions: {
        fetchReviews({commit}) {
            axios.get(route('auth.reviews.index')).then(response => {
                commit('setReviews', response.data);
            });
        }
    },
    mutations: {
        setReviews(state, reviews) {
            state.reviews = reviews;
        }
    }
}