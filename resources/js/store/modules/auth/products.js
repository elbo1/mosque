import axios from 'axios';

export default {
    namespaced: true,
    state: {
        products: [],
        selected_product: {
            name: '',
            description: '',
            price: ''
        },
    },
    actions: {
        fetchProducts({commit}) {
            axios.get(route('auth.products.index')).then(response => {
                commit('setProducts', response.data);
            });
        },
        fetchProduct({commit}, product_id) {
            axios.get(route('product.show', {product: product_id})).then(response => {
                commit('setSelectedProduct', response.data);
            });
        },
        resetSelectedProduct({commit}) {
            commit('setSelectedProduct', {
                name: '',
                description: '',
                price: ''
            });
        }
    },
    mutations: {
        setProducts(state, products) {
            state.products = products;
        },
        setSelectedProduct(state, product) {
            state.selected_product = product;
        }
    }
}