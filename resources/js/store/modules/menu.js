export default {
    namespaced: true,
    state: {
        drawer: true
    },
    actions: {
        setDrawer({commit}, drawer) {
            commit('setDrawer', drawer);
        }
    },
    mutations: {
        setDrawer(state, drawer) {
            state.drawer = drawer;
        }
    }
}