import {mapState, mapActions} from 'vuex';

export default {
    computed: {
        ...mapState('menu', {
            state_drawer: 'drawer'
        }),
        drawer: {
            set(drawer) {

                drawer = this.$vuetify.breakpoint.smAndDown ? ! drawer : drawer;

                this.setDrawer(drawer);
            },
            get() {
                return this.$vuetify.breakpoint.smAndDown ? ! this.state_drawer : this.state_drawer;
            }
        }
    },
    methods: {
        ...mapActions({
            setDrawer: 'menu/setDrawer'
        }),
    }
}
