<?php

namespace App\Http\Controllers;

use App\Http\Resources\CityResource;
use App\Models\City;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CityController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return CityResource::collection(City::whereHas('shopContacts')->get());
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function overview(): AnonymousResourceCollection
    {
        return CityResource::collection(City::all());
    }

    /**
     * @param City $city
     *
     * @return CityResource
     */
    public function show(City $city): CityResource
    {
        return CityResource::make($city);
    }

    /**
     * @param Request $request
     *
     * @return CityResource
     */
    public function create(Request $request): CityResource
    {
        $city = $this->buildCity($request);

        return CityResource::make($city);
    }

    /**
     * @param Request $request
     * @param City    $city
     *
     * @return CityResource
     */
    public function update(Request $request, City $city): CityResource
    {
        $city = $this->buildCity($request, $city);

        return CityResource::make($city);
    }

    /**
     * @param City|null $city
     *
     * @return City
     */
    private function buildCity(Request $request, ?City $city = null): City
    {
        $validated_data = $request->validate([
            'name' => 'required|string',
            'province_id' => 'required|numeric',
        ]);

        if (! $city) {
            $city = new City();
        }

        $city->name = $validated_data['name'];
        $city->province()->associate(Province::find($validated_data['province_id']));
        $city->save();

        return $city;
    }
}
