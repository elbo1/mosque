<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProvinceResource;
use App\Models\Province;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProvinceController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return ProvinceResource::collection(Province::whereHas('cities.shopContacts')->get());
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function overview(): AnonymousResourceCollection
    {
        return ProvinceResource::collection(Province::all());
    }
}
