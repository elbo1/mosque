<?php

namespace App\Spot\Helpers;

use Carbon\Carbon;

class Spot
{
    /**
     * @return int
     */
    public static function dayOfWeek(): int
    {
        return Carbon::now()->dayOfWeekIso - 1;
    }

    /**
     * @return int
     */
    public static function yesterdayOfWeek(): int
    {
        $day_of_week = self::dayOfWeek() - 1;

        if ($day_of_week < 0) {
            $day_of_week = 6;
        }

        return $day_of_week;
    }

    public static function capitalize(string $name): string
    {
        return ucfirst(strtolower($name));
    }
}
