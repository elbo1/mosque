<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopReview extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $casts = [
        'quality' => 'float',
        'price'   => 'float',
        'service' => 'float',
        'allow'   => 'boolean'
    ];

    protected static function boot(): void
    {
        parent::boot();

        static::addGlobalScope(static function (Builder $builder) {
            $builder->where('allow', true);
            $builder->orderByDesc('created_at');
        });
    }

    /**
     * @return BelongsTo
     */
    public function shop(): BelongsTo
    {
        return $this->belongsTo(Shop::class);
    }
}
