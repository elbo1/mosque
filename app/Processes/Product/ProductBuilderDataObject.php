<?php

namespace App\Processes\Product;

class ProductBuilderDataObject
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var float
     */
    private $price;

    /**
     * @var float
     */
    private $deposit;

    public function __construct(string $name, string $description, float $price, float $deposit)
    {
        $this->name        = $name;
        $this->description = $description;
        $this->price       = $price;
        $this->deposit     = $deposit;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getDeposit(): float
    {
        return $this->deposit;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}