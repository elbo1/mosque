<?php

/* @var $factory Factory */

use App\Model;
use App\Models\Shop;
use App\Models\ShopReview;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(ShopReview::class, static function (Faker $faker) {
    return [
        'shop_id' => static function () {
            return factory(Shop::class)->create()->id;
        },
        'name'    => $faker->name,
        'email'   => $faker->unique()->email,
        'quality' => static function () use ($faker) {
            return floor($faker->randomFloat(2, 0, 5) * 2) / 2;
        },
        'price'   => static function () use ($faker) {
            return floor($faker->randomFloat(2, 0, 5) * 2) / 2;
        },
        'service' => static function () use ($faker) {
            return floor($faker->randomFloat(2, 0, 5) * 2) / 2;
        },
        'comment' => $faker->text,
        'ip_address' => $faker->ipv4,
        'allow' => 1,

    ];
});
