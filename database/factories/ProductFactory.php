<?php

/* @var $factory Factory */

use App\Model;
use App\Models\Product;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Product::class, static function (Faker $faker) {
    $price = floor($faker->randomFloat(2, 0, 500) * 2) / 2;

    return [
        'name'        => $faker->word,
        'description' => $faker->text,
        'price'       => $price,
        'deposit'     => floor($faker->randomFloat(2, 0, $price * 0.75) * 2) / 2
    ];
});
