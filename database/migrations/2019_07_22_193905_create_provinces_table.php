<?php

use App\Models\City;
use App\Models\Province;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvincesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provinces', static function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::table('cities', static function (Blueprint $table) {
            $table->unsignedInteger('province_id')->after('name');
        });

        $provinces = [
            [
                'name' => 'Noord-Holland',
            ],
            [
                'name' => 'Flevoland',
            ],
            [
                'name' => 'Gelderland',
            ],
            [
                'name' => 'Utrecht',
            ],
            [
                'name' => 'Zeeland',
            ],
            [
                'name' => 'Noord-Brabant',
            ],
            [
                'name' => 'Limburg',
            ],
            [
                'name' => 'Friesland',
            ],
            [
                'name' => 'Groningen',
            ],
            [
                'name' => 'Zuid-Holland',
            ],
            [
                'name' => 'Drenthe',
            ],
            [
                'name' => 'Overijssel',
            ],
        ];

        Province::insert($provinces);

        City::all()->each(static function (City $city) {
            switch ($city->name) {
                case 'Hilversum':
                    $city->province()->associate(Province::where('name', 'Noord-Holland')->first());
                    break;
                case 'Amsterdam':
                    $city->province()->associate(Province::where('name', 'Noord-Holland')->first());
                    break;
                case 'Almere':
                    $city->province()->associate(Province::where('name', 'Flevoland')->first());
                    break;
                case 'Lelystad':
                    $city->province()->associate(Province::where('name', 'Flevoland')->first());
                    break;
                case 'Utrecht':
                    $city->province()->associate(Province::where('name', 'Utrecht')->first());
                    break;
                case 'Leeuwarden':
                    $city->province()->associate(Province::where('name', 'Friesland')->first());
                    break;
                case 'Delft':
                    $city->province()->associate(Province::where('name', 'Zuid-Holland')->first());
                    break;
                case 'Roermond':
                    $city->province()->associate(Province::where('name', 'Limburg')->first());
                    break;
                case 'Eindhoven':
                    $city->province()->associate(Province::where('name', 'Noord-Brabant')->first());
                    break;
                case 'Rotterdam':
                    $city->province()->associate(Province::where('name', 'Zuid-Holland')->first());
                    break;
                case 'Den Haag':
                    $city->province()->associate(Province::where('name', 'Zuid-Holland')->first());
                    break;
                case 'Tilburg':
                    $city->province()->associate(Province::where('name', 'Noord-Holland')->first());
                    break;
                case 'Weesp':
                    $city->province()->associate(Province::where('name', 'Noord-Holland')->first());
                    break;
                case 'Arnhem':
                    $city->province()->associate(Province::where('name', 'Gelderland')->first());
                    break;
                case 'Hengelo':
                    $city->province()->associate(Province::where('name', 'Overijssel')->first());
                    break;
                case 'Haarlem':
                    $city->province()->associate(Province::where('name', 'Noord-Holland')->first());
                    break;
                case 'Enschede':
                    $city->province()->associate(Province::where('name', 'Overijssel')->first());
                    break;
                case 'Vlissingen':
                    $city->province()->associate(Province::where('name', 'Zeeland')->first());
                    break;
                case 'Zaandam':
                    $city->province()->associate(Province::where('name', 'Noord-Holland')->first());
                    break;
                case 'Alkmaar':
                    $city->province()->associate(Province::where('name', 'Noord-Holland')->first());
                    break;
                case 'Nieuwegein':
                    $city->province()->associate(Province::where('name', 'Utrecht')->first());
                    break;
            }
            $city->save();
        });

        Schema::table('cities', static function (Blueprint $table) {
            $table->foreign('province_id')->on('provinces')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provinces');
    }
}
