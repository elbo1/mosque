<?php

use App\Models\City;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->softDeletes();
        });

        $cities = [
            [
                'name' => 'Hilversum',
            ], [
                'name' => 'Amsterdam',
            ], [
                'name' => 'Almere'
            ], [
                'name' => 'Utrecht',
            ], [
                'name' => 'Leeuwarden',
            ], [
                'name' => 'Delft',
            ], [
                'name' => 'Roermond',
            ], [
                'name' => 'Lelystad',
            ], [
                'name' => 'Eindhoven',
            ], [
                'name' => 'Rotterdam',
            ], [
                'name' => 'Den Haag',
            ], [
                'name' => 'Tilburg',
            ], [
                'name' => 'Weesp',
            ], [
                'name' => 'Arnhem',
            ], [
                'name' => 'Hengelo',
            ], [
                'name' => 'Haarlem',
            ], [
                'name' => 'Enschede',
            ], [
                'name' => 'Vlissingen',
            ], [
                'name' => 'Zaandam',
            ], [
                'name' => 'Alkmaar',
            ], [
                'name' => 'Nieuwegein',
            ],

        ];

        City::insert($cities);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
