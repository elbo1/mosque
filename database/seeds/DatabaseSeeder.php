<?php

use App\Models\City;
use App\Models\Product;
use App\Models\Shop;
use App\Models\ShopContact;
use App\Models\ShopOpeningHour;
use App\Models\ShopReview;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Faker\Generator;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /**
         * @var Collection $shops
         * @var Collection $products
         * @var Generator  $faker
         */
        $shops    = factory(Shop::class, 10)->create();
        $products = factory(Product::class, 15)->create();
        $faker    = resolve(Generator::class);
        $cities   = City::all();

        /**
         * @var Shop $shop
         */
        foreach ($shops as $shop) {
            $shop_contact    = factory(ShopContact::class)->create(['shop_id' => $shop->id]);
            $random_cities   = $cities->random($faker->numberBetween(1, 3));
            $random_products = $products->random($faker->numberBetween(1, 5));

            $random_cities = $random_cities->mapWithKeys(static function (City $city, int $index) {
                return [$city->id => ['primary' => $index === 0]];
            })->toArray();

            $shop_contact->cities()->sync($random_cities);
            $shop->products()->sync($random_products->pluck('id'));

            factory(ShopOpeningHour::class, 7)
                ->make(['shop_id' => $shop->id])
                ->map(static function (ShopOpeningHour $opening_hour, int $index) {
                    $opening_hour->day = $index;
                    $opening_hour->save();

                    return $opening_hour;
                });

            factory(ShopReview::class, $faker->numberBetween(0, 20))->create(['shop_id' => $shop->id]);
        }
    }
}
